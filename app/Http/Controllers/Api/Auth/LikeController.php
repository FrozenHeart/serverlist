<?php

namespace App\Http\Controllers\Api\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Cog\Contracts\Love\ReactionType\Models\ReactionType;
use App\Server;
use App\User;
use DB;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $reacter = auth()->user()->getLoveReacter();
        $server = Server::findOrFail($id);
        $reactant = $server->getLoveReactant();
        $reactionType = DB::table('love_reaction_types')->where('name', 'Like')->first();

        if ($reacter->isReactedTo($reactant)) {
            $reaction = $reacter->reactions()->where([
                'reaction_type_id' => $reactionType->id,
                'reactant_id' => $reactant->id,
            ])->first();
            $reaction->delete();
        }
        else {
            $reacter->reactions()->create([
                'reaction_type_id' => $reactionType->id,
                'reactant_id' => $reactant->id,
            ]);
        }
        $server->save();



        return $reactant->getReactionTotal()->getCount();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
