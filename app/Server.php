<?php

namespace App;

use Cog\Contracts\Love\Reactable\Models\Reactable as ReactableContract;
use Cog\Laravel\Love\Reactable\Models\Traits\Reactable;
use Illuminate\Database\Eloquent\Model;

class Server extends Model implements ReactableContract
{
    use Reactable;

    protected $fillable = ['user_id', 'name', 'description', 'usersmin', 'usersmax', 'category_id', 'version', 'image'];

    public function user(): BelongsToo
    {
        return $this->belongsTo(User::class);
    }
}
