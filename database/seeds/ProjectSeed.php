<?php

use Illuminate\Database\Seeder;

class ProjectSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Server::create([
            'user_id' => '1',
            'name' => 'Servidor RP',
            'description' => 'Descripción del servidor',
            'usersmin' => '100',
            'usersmax' => '200',
            'category' => '0',
            'version' => '0',
            'image' => 'https://i.ytimg.com/vi/jKzvJhmcBKs/maxresdefault.jpg'
        ]);
        App\Server::create([
            'user_id' => '1',
            'name' => 'Servidor FR',
            'description' => 'ASOPUTAMADRE',
            'usersmin' => '5',
            'usersmax' => '10',
            'category' => '0',
            'version' => '0',
            'image' => 'https://i.ytimg.com/vi/jKzvJhmcBKs/maxresdefault.jpg'
        ]);
    }
}
