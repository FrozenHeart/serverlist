<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class EditServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up(): void
     {
         Schema::table('servers', function (Blueprint $table) {
             $table->unsignedBigInteger('love_reactant_id')->nullable();
         });
     }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
