import React from 'react';
import { Link } from 'react-router-dom';

const GuestNav = () => {
  return (
      <header>
          <nav className="navbar navbar-expand-md navbar-dark  scrolling-navbar blue">
              <div className="container">
                  <Link className="navbar-brand font-weight-bold title" to="/">{window.App.name}</Link>
                  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                      <span className="navbar-toggler-icon"></span>
                  </button>
                  <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                      <ul className="list-unstyled navbar-nav mr-auto">
                          <li className="nav-item ml-4 mb-0">
                              <Link className="nav-link title waves-effect waves-light" to="/">Inicio</Link>
                          </li>
                          <li className="nav-item ml-4 mb-0">
                              <Link className="nav-link title waves-effect waves-light" to="/signin">Ingreso</Link>
                          </li>
                          <li className="nav-item">
                              <Link className="nav-link" to="/register">Registro</Link>
                          </li>
                      </ul>
                      <ul className="navbar-nav nav-flex-icons">
                          <li className="nav-item">
                              <a className="nav-link waves-effect waves-light">
                                  <i className="fa fa-facebook title"></i>
                              </a>
                          </li>
                          <li className="nav-item">
                              <a className="nav-link waves-effect waves-light">
                                  <i className="fa fa-twitter title"></i>
                              </a>
                          </li>
                          <li className="nav-item">
                              <a className="nav-link waves-effect waves-light">
                                  <i className="fa fa-instagram title"></i>
                              </a>
                          </li>
                          <li className="nav-item ml-3">
                              <a className="btn pink-gradient btn-rounded btn-sm font-weight-bold waves-effect waves-light" href="#contact" data-offset="90">Free trial</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
      </header>
  );
};

export default GuestNav;
