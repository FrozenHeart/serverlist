import React from 'react';

const Footer = () => (
    <footer className="page-footer stylish-color-dark">
        <div className="container-fluid">
            <div className="row d-flex align-items-center">
                <div className="col-md-7 col-lg-8">
                    <p className="text-center text-md-left grey-text " style={{marginTop: 20 + 'px'}}>
                        &copy;{(new Date()).getFullYear()} <a href="https://mdbootstrap.com/education/bootstrap/" target="_blank"> ForwardDevs </a>
                    </p>
                </div>
                <div className="col-md-5 col-lg-4 ml-lg-0">
                    <div className="social-section text-center mr-auto text-md-left " style={{marginTop: 20 + 'px'}}>
                        <ul className="list-unstyled list-inline">
                            <li className="list-inline-item">
                                <a className="btn-floating btn-sm rgba-white-slight">
                                    <i className="fa fa-facebook"></i>
                                </a>
                            </li>
                            <li className="list-inline-item">
                                <a className="btn-floating btn-sm rgba-white-slight">
                                    <i className="fa fa-twitter"></i>
                                </a>
                            </li>
                            <li className="list-inline-item">
                                <a className="btn-floating btn-sm rgba-white-slight">
                                    <i className="fa fa-google-plus"></i>
                                </a>
                            </li>
                            <li className="list-inline-item">
                                <a className="btn-floating btn-sm rgba-white-slight">
                                    <i className="fa fa-linkedin"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </footer>
);

export default Footer;
