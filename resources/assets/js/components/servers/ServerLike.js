import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class ServerLike extends Component {
    constructor(props) {
        super(props);
        this.state = {
          likes: this.props.likes
        };
    }
    handleClick(param) {
        this.props.likeServer.call(this);
    }

    render() {
      return (
        <div>
          <a onClick={this.handleClick.bind(this)}><h4><span className="pull-right badge badge-secondary"><i className="fa fa-heart"></i> {this.props.likes}</span></h4></a>
        </div>
      )
    }
}
export default ServerLike;