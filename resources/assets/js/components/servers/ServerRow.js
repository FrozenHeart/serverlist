import React, { Component } from 'react';
import store from '../../store';
import ServerLike from './ServerLike';
import { Link } from 'react-router-dom';

class ServerRow extends Component {
    constructor(props) {
        super(props);
        this.state = {
          number: this.props.number,
          server: this.props.server
        };
    }

    likeServer() {
      window.axios.get('/api/servers/like/'+this.state.server.id).then(response => {

        this.setState(prevState => ({
          server: {
              ...prevState.server,
              reactions_total_count: response.data
          },
        }));

      });
    }
    render () {
        const { server } = this.state;
        const { auth: { authenticated } } = store.getState();
        return (
            <tr className="table-primary">
                <th width="10%" scope="row" className="align-middle"> <h4><span className="badge badge-secondary"># {this.state.number+1}</span></h4> </th>
                <td width="20%" className="align-middle"><img src={server.image} width="120" height="80" className="img-fluid" alt="sample image"/></td>
                <td width="50%" className="align-middle"><h5 className="mt-0"><Link to='/'><strong>{server.name}</strong></Link></h5><small className="grey-text">{server.description}</small></td>
                <td width="10%" className="align-middle" style={{textAlign: 'center'}}><span className="badge badge-warning">0.3.7</span><span className="badge badge-info"><i className="fa fa-users"></i> {server.usersmin}-{server.usersmax} </span></td>
                <td width="10%" className="align-middle">  {authenticated ? <ServerLike likeServer={this.likeServer.bind(this)} likes={server.reactions_total_count} /> : <Link to="/signin"><h4><span className="pull-right badge badge-secondary"><i className="fa fa-heart"></i> {server.reactions_total_count}</span></h4></Link>}</td>
            </tr>
        );
    }
}

export default ServerRow;
