import React, { Component } from 'react';
import update from 'react-addons-update';
import ServerRow from './ServerRow';



class ServerTable extends Component {
  constructor (props) {
    super(props);
    this.state = {
      servers: []
    };
  }

  componentDidMount () {
    axios.get('/api/servers').then(response => {
      this.setState({
        servers: response.data
      })
    })
  }

 
  
  render() {
    const { servers } = this.state;
    return (
        <div>
            <table className="table">
                <tbody>
                {servers.map((server, index) => (
                    <ServerRow 
                    server={server} 
                    key={index} 
                    number={index}  />
                ))}
                </tbody>
            </table>
        </div>
    );
  }
  


  
}
export default ServerTable;
