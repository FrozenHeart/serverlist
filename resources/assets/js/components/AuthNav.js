import React, { Component } from 'react';
import { NavLink, Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { logoutUser } from '../actions/auth';

const propTypes = {
  auth: PropTypes.object.isRequired,
  logoutUser: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

class AuthNav extends Component {
  constructor (props) {
    super(props);
    this.state = {
      user: this.props.auth.user,
      hideMobileNav: true
    };
  }

  toggleMobileNav () {
    this.setState((prevState, props) => ({
      hideMobileNav: !prevState.hideMobileNav
    }));
  }

  closeMobileNav () {
    if (!this.state.hideMobileNav) {
      this.setState({
        hideMobileNav: true
      });
    }
  }

  handleLogout () {
    this.props.logoutUser(() => this.props.history.push('/'));
  }

  render () {
    return (
        <header>
            <nav className="navbar navbar-expand-md navbar-dark  scrolling-navbar blue">
                <div className="container">
                    <Link className="navbar-brand font-weight-bold title" to="/">{window.App.name}</Link>
                    <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul className="list-unstyled navbar-nav mr-auto">
                            <li className="nav-item ml-4 mb-0">
                                <Link className="nav-link title waves-effect waves-light" to="/">Inicio</Link>
                            </li>

                                <li className="nav-item dropdown">
                                    <a id="navbarDropdown" className="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" >
                                        {this.state.user.name} <span className="caret"></span>
                                    </a>

                                    <div className="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                        <ul className="list-unstyled">
                                            <li>
                                        <Link to="/home" className="dropdown-item" >
                                            Perfil
                                        </Link>
                                        </li>
                                        <li>

                                            <Link to="/logout" onClick={() => this.handleLogout()} className="dropdown-item" >
                                                Salir
                                            </Link>
                                    </li>
                                        </ul>
                                    </div>



                                </li>

                        </ul>
                        <ul className="navbar-nav nav-flex-icons">
                            <li className="nav-item">
                                <a className="nav-link waves-effect waves-light">
                                    <i className="fa fa-facebook title"></i>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link waves-effect waves-light">
                                    <i className="fa fa-twitter title"></i>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link waves-effect waves-light">
                                    <i className="fa fa-instagram title"></i>
                                </a>
                            </li>
                            <li className="nav-item ml-3">
                                <a className="btn pink-gradient btn-rounded btn-sm font-weight-bold waves-effect waves-light" href="#contact" data-offset="90">Free trial</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
        </header>
    );
  }
}

AuthNav.propTypes = propTypes;

const mapDispatchToProps = { logoutUser };
const mapStateToProps = ({ auth }) => ({ auth });

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { pure: false }
)(withRouter(AuthNav));
