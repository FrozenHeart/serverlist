import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import AuthNav from '../components/AuthNav';
import GuestNav from '../components/GuestNav';
import Footer from '../components/Footer';
import { Link } from 'react-router-dom';
import store from '../store';
import ServerTable from '../components/servers/ServerTable';
class Welcome extends Component {

    constructor () {
        super()
        this.state = {
          servers: []
        }
      }


      render () {
          const { auth: { authenticated } } = store.getState();
          return (
              <DocumentTitle title={`Inicio - ${window.App.name}`}>
                  <div className="content">
                      {authenticated ? <AuthNav /> : <GuestNav />}

                          <div className="container-fluid" style={{minHeight: '550px'}}>
                                <div className="row justify-content-center">
                                    <div className="col-xl-8 col-md-12 pl-0">
                                        <section className="section widget-content mt-5">

                                            <ul className="nav md-tabs tabs-3 nav-justified widget-tabs mdb-color lighten-3" role="tablist">
                                                <li className="nav-item">
                                                    <a className="nav-link active px-0" data-toggle="tab" href="#panel1" role="tab">ROLEPLAY</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link" data-toggle="tab" href="#panel2" role="tab">DEATHMATCH</a>
                                                </li>
                                                <li className="nav-item">
                                                    <a className="nav-link" data-toggle="tab" href="#panel3" role="tab">FREEROAM</a>
                                                </li>
                                            </ul>

                                            <div className="tab-content widget-tabs-content ml-2">

                                                <div className="tab-pane fade in show active" id="panel1" role="tabpanel">

                                                    <ServerTable />

                                                </div>


                                                <div className="tab-pane fade" id="panel2" role="tabpanel">



                                                </div>

                                                <div className="tab-pane fade" id="panel3" role="tabpanel">


                                                </div>
                                            </div>
                                        </section>
                                    </div>
                                    <div className="col-xl-4 col-md-12 widget-column mt-0">
                                        <section className="section mb-5">

                                                <br/>

                                                <ul className="list-group z-depth-1 mt-4">
                                                    <li className="list-group-item d-flex justify-content-center align-items-center">
                                                        <Link to='/'>Todos los servidores</Link>
                                                    </li>
                                                    <li className="list-group-item d-flex justify-content-center align-items-center">
                                                        <Link to='/'><img src="https://img.icons8.com/ios/32/000000/san-andreas-filled.png"/> Version 0.3.7</Link>
                                                    </li>
                                                    <li className="list-group-item d-flex justify-content-center align-items-center">
                                                        <Link to='/'><img src="https://img.icons8.com/nolan/35/000000/san-andreas.png"/> Version 0.3.DL</Link>
                                                    </li>
                                                </ul>
                                            </section>
                                    </div>
                                </div>
                            </div>

                      <Footer />
                  </div>
              </DocumentTitle>
          );

      }
};

export default Welcome;
