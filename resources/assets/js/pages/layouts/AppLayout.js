import React from 'react';
import AuthNav from '../../components/AuthNav';
import Footer from '../../components/Footer';

import PropTypes from 'prop-types';

const propTypes = {
  children: PropTypes.element.isRequired
};

const AppLayout = ({ children, ...rest }) => {
  return (
    <div>
      <AuthNav />

        {children}

      <Footer />
    </div>
  );
};

AppLayout.propTypes = propTypes;

export default AppLayout;
