import React, { Component } from 'react';
import { Link, withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import DocumentTitle from 'react-document-title';
import { signInUser, googleSignIn } from '../../actions/auth';
import { getIntendedUrl } from '../../helpers/auth';
import { destructServerErrors, hasError, getError } from '../../helpers/error';
import GoogleSignIn from '../../components/GoogleSignIn';
import GuestNav from '../../components/GuestNav';
import Footer from '../../components/Footer';

const propTypes = {
  signInUser: PropTypes.func.isRequired,
  googleSignIn: PropTypes.func.isRequired,
  history: PropTypes.object.isRequired
};

class SignIn extends Component {
  constructor (props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      errors: ''
    };
  }

  signInSuccess () {
    getIntendedUrl().then(url => this.props.history.push(url));
  }

  handleSubmit (e) {
    e.preventDefault();
    this.props.signInUser(this.state)
      .then(response => this.signInSuccess())
      .catch(error => this.setState({ errors: destructServerErrors(error) }));
  }

  handleInputChange (e) {
    this.setState({
      [e.target.name]: e.target.value,
      errors: {
        ...this.state.errors,
        ...{ [e.target.name]: '' }
      }
    });
  }

  handleGoogleSignInSuccess (credentials) {
    this.props.googleSignIn(credentials)
      .then(response => this.signInSuccess())
      .catch(error => this.setState({ errors: destructServerErrors(error) })); ;
  }

  render () {
    return (
        <DocumentTitle title={`Ingreso - ${window.App.name}`}>
            <div>
                <GuestNav />
                <div className="container-fluid mt-5">
                    <div className="row justify-content-center">
                        <div className="col-md-8">
                            <div className="card">
                                <div className="card-header">Login</div>
                                <div className="card-body">
                                    <form role="form" method="POST" onSubmit={e => this.handleSubmit(e)}>
                                        <div className="form-group row">
                                            <label htmlFor="email" className="col-md-4 col-form-label text-md-right">Correo Electrónico</label>

                                            <div className="col-md-6">
                                                <input value={this.state.email} onChange={e => this.handleInputChange(e)} id="email" type="email"  className="form-control ${hasError(this.state.errors, 'email') ? 'border-red' : ''}" name="email"   required />
                                                {hasError(this.state.errors, 'email') &&
                                                    <p className="text-red text-xs pt-2">{getError(this.state.errors, 'email')}</p>
                                                }
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <label htmlFor="password" className="col-md-4 col-form-label text-md-right">Contraseña</label>

                                            <div className="col-md-6">
                                                <input
                                                    value={this.state.password}
                                                    onChange={e => this.handleInputChange(e)}
                                                    type="password"
                                                    id="password"
                                                    name="password"
                                                    className="form-control"
                                                    required />
                                            </div>
                                        </div>

                                        <div className="form-group row">
                                            <div className="col-md-6 offset-md-4">
                                                <div className="form-check">

                                                    <input className="form-check-input" type="checkbox" name="remember" />
                                                    <label className="form-check-label" htmlFor="remember">
                                                        Recuerdame
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div className="form-group row mb-0">
                                            <div className="col-md-8 offset-md-4">
                                                <button type="submit" className="btn btn-primary" id="email-login-btn">
                                                    Login
                                                </button>

                                                <li className="btn btn-link">
                                                    <Link to = "forgotpassword">¿Olvidaste tu contraseña?</Link>
                                                </li>
                                            </div>
                                        </div>
                                    </form>
                                    <div className="p-4 text-grey-dark text-sm flex flex-col items-center">
                                        <div>
                                            <span>¿Eres nuevo? </span>
                                            <Link to="/register" className="no-underline text-grey-darker font-bold">Registrate</Link>
                                        </div>

                                        <div className="mt-2">
                                            <strong>Ayuda:</strong> <Link to="/forgot-password" className="no-underline text-grey-dark text-xs">Recuperar contraseña</Link>
                                        </div>
                                    </div>

                                    <div className="border justify-content-center rounded bg-white border-grey-light w-3/4 sm:w-1/2 lg:w-2/5 xl:w-1/4 px-8 py-4">
                                        <center><GoogleSignIn googleSignInSuccess={(credentials) => this.handleGoogleSignInSuccess(credentials)} /></center>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <Footer />
            </div>
        </DocumentTitle>
    );
  }
}

SignIn.propTypes = propTypes;

const mapDispatchToProps = {
  signInUser,
  googleSignIn
};

export default connect(null, mapDispatchToProps)(withRouter(SignIn));
