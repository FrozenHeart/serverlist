import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AuthNav from '../components/AuthNav';
import GuestNav from '../components/GuestNav';

const propTypes = {
  authenticated: PropTypes.bool.isRequired
};

const NotFound = (props) => {
  const nav = props.authenticated ? <AuthNav /> : <GuestNav />;

  return (
      <div>
          {nav}

          <div className="container-fluid mt-5">
              <div className="row justify-content-center">
                  <div className="col-md-8">
                      <div className="card">
                          <div className="card-header">Oops, algo esta mal</div>
                          <div className="card-body">
                              <p className="text-grey-dark">
                                  No encontramos el sitio que buscabas, puede que la ruta este mal.
                              </p>
                          </div>
                      </div>
                  </div>

              </div>
          </div>
      </div>
  );
};

NotFound.propTypes = propTypes;
const mapStateToProps = ({ auth: { authenticated } }) => ({ authenticated });
export default connect(mapStateToProps)(NotFound);
