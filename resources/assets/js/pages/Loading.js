import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import AuthNav from '../components/AuthNav';
import GuestNav from '../components/GuestNav';
import '../css/style.css';

const propTypes = {
  authenticated: PropTypes.bool.isRequired
};

const Loading = (props) => {
  const nav = props.authenticated ? <AuthNav /> : <GuestNav />;

  return (
      <div>
          {nav}
          <div className="container-fluid mt-5">
              <div className="row justify-content-center">
                  <div className="col-md-8" style={{textAlign: 'center'}}>
                      <h1>Cargando</h1>
                      <div className="loading"></div>
                  </div>
              </div>
          </div>
      </div>
  );
};

Loading.propTypes = propTypes;
const mapStateToProps = ({ auth: { authenticated } }) => ({ authenticated });
export default connect(mapStateToProps)(Loading);
